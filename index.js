/*
	First, we load the expressjs module into our application and saved it in a variable called express.
*/
const express = require("express");

/*
	Create an application with expressjs
	This creates an application that uses express and stores it as app
	app is our server
*/
const app = express();

//port is just a variable to contain the port number we want to designate to our server
const port = 4000;

//express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our request's body.
//app.use() is used to run a method function for our expressjs api.
app.use(express.json());

//mock data
let users = [

	{
		username: "tinaRCBC",
		email: "tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},
	{
		username: "gwenstacy1",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	},
	

];


//app.listen() allows us to designated the server to run on the indicated port number and run console log afterwards.

//Express has methods to use as routes corresponding to each HTTP method.
//app.get(<endpoint>,<function handling request and response>)

app.get('/',(req,res)=>{

	//Once the route is accessed, we can send a respons with the use of res.send()
	//res.senr() actually combines our writeHead() and end().
	//It is used to send a response to the client and ends the response.
	res.send("Hello World from our first ExpressJS API!");

})

app.get('/hello', (req,res)=>{

	res.send("Hello from Batch 152!");

})

app.get('/users', (req,res)=>{

	//res.send() already stringifies for you.
	res.send(users);

})

//How do we get data from the client as a request body?
app.post('/users', (req,res)=>{

	//With the use of express.json(), we simply have to access the body property of our request object to get the body/input of the request.
	//The receiving of data and the parsing of JSON to JS object has already been done by express.json()

	//Note: When dealing with a route that receives data from request body is a good practice to check the incoming data from the client.
	console.log(req.body);

	//simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	//push newUser into the users array
	users.push(newUser);
	console.log(users);

	//send the updated the users array in the client
	res.send(users);

})


//delete user route
app.delete('/users', (req,res)=>{

	users.pop();
	console.log(users);

	res.send(users);

})

//update user route
app.put('/users/:index', (req,res)=>{

	//Updating our user will require us to add an input from our client
	//req.body will contain the password.
	console.log(req.body);

	//req.params object which contains the value of the url params.
	//url params is captured by route parameter (:parameterName) and saved as a property in req.params
	console.log(req.params);

	//parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	//get the item we want to update with our index number from our URL params
	users[index].password = req.body.password;

	//send the updated user to the client.
	//provide the index variable to be the index for the particular item in the array.
	res.send(users[index]);


/*	//get the index from your request body first
	users[req.body.index].password = req.body.password

	//send the updated user to client
	res.send(users[req.body.index]);*/

})

//get details of single user
//GET method request should not have a request body. It may have headers for additional information. We can pass small amount of data somewhere else: Through the url.
//Route params are values we can pass via the URL.
//This is done specifically to allow us to send small amount of data into our server through the request URL.
//Route parameters can be defined in the endpoint of a route with :parameterName
app.get('/users/getSingleUser/:index', (req,res)=>{

	//URL: http://localhost:4000/users/getSingleUser/0
	//req.params is an object that contains the values passed as route params.
	//In req.params, the parameter name given in the root endpoint becomes a property name.
	//Our URL is a string.
	console.log(req.params);
	/*
		{ index: '0'}

	*/

	//parseInt() the value of req.params.index to convert the value from string to number.
	let index = parseInt(req.params.index);
	console.log(index);

	//send the user with the appropriate index number
	res.send(users[index])

})




//Activity

let items = [

	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},
	{
		name: "Doritos",
		price: 150,
		isActive: true
	}

];


app.get('/items', (req,res)=>{

	res.send(items);

})

app.post('/items', (req,res)=>{

	console.log(req.body);

	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);
	console.log(items);

	res.send(items);

})

app.put('/items/:index', (req,res)=>{

	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);

})




//Activity

app.get('/items/getSingleItem/:index', (req,res)=>{

	let index = parseInt(req.params.index);

	res.send(items[index]);

})

app.put('/items/archive/:index', (req,res)=>{

	let index = parseInt(req.params.index);


	if (items[index].isActive === true){

		items[index].isActive = false;
		res.send(items[index]);

	} else {

		res.send("Item is not active, thus cannot be archived.");

	}

	console.log(items);

})

app.put('/items/activate/:index', (req,res)=>{

	let index = parseInt(req.params.index);


	if (items[index].isActive === false){

		items[index].isActive = true;
		res.send(items[index]);

	} else {

		res.send("Item is already available for selling, thus cannot be activated.");

	}

	console.log(items);

})



app.listen(port,()=>console.log(`Server is running at port ${port}`));